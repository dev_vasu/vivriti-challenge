import React from 'react';
import Add from '@material-ui/icons/Add';
import Remove from '@material-ui/icons/Remove';
import Card from '@material-ui/core/Card';

export default class SingleItem extends React.Component {
  render() {
    return (
      <Card className="single-item">
        <div>
          <div className='head'>
            <div className='head-name'>
              <p>{`#${this.props.item.id} - ${this.props.item.name}`}</p>
            </div>
            <div className='head-add'>
              <Add
                onClick={e => {
                  this.props.add(this.props.item.name);
                }}
              />
            </div>
            <p className='head-count'>{this.props.shoppingCart[this.props.item.name]}</p>
            <div className='head-remove'>
              <Remove
                onClick={e => {
                  this.props.remove(this.props.item.name);
                }}
              />
            </div>
          </div>
        </div>

        <div className='item-body'>
          <div className='item-style'>
            <p>{'Style : ' + this.props.item.style}</p>
          </div>
          <div className='item-abv'>
            <p>{'ABV : '}{this.props.item.abv || 'No ABV'}</p>
          </div>
          <div className='item-ibu'>
            <p>{'IBU : '}{this.props.item.ibu || 'No IBU'}</p>
          </div>
          <div className='item-ounces'>
            <p>{'OUNCES : '}{this.props.item.ounces || 'No Ounces'}</p>
          </div>
        </div>

      </Card >
    );
  }
}
