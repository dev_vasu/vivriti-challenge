import React from 'react';
import CenterPanel from '../center-panel';
import RightPanel from '../right-panel';

const NewHome = () => (
  <div className="container">
    <CenterPanel />
    <RightPanel />
  </div>
);

export default NewHome;
