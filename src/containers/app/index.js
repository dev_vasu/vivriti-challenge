import React from 'react';
import { Route } from 'react-router-dom';
import NewHome from '../new-home';
import About from '../about';
import Header from '../header';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateFoodItems } from '../../modules/foodItems';
import axios from 'axios';

class App extends React.Component {
  componentWillMount() {
    var that = this;
    axios
      .get('http://starlord.hackerearth.com/beercraft')
      .then(function(response) {
        that.props.updateFoodItems(response.data);
        console.log(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <Header />
        <main>
          <Route exact path="/" component={NewHome} />
          <Route exact path="/about-us" component={About} />
        </main>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  initialItems: state.foodItems.initialItems
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateFoodItems
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
