import React from 'react';

export default class SingleCartItem extends React.Component {
  render() {
    return (
      <div>
        {Boolean(this.props.shoppingCart[this.props.item.name]) && (
          <div className="single-cart-item">
            <p>{this.props.item.name}</p>
            <p>{this.props.shoppingCart[this.props.item.name]}</p>
          </div>
        )}
      </div>
    );
  }
}
