import React from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import SingleCartItem from './single-cart-item';
import { addFoodItems, removeFoodItems } from '../../modules/foodItems';
import { bindActionCreators } from 'redux';

class RightPanel extends React.Component {
  render() {
    return (
      <div>
        <List component="nav">
          {this.props.foodItems.map((item, index) => {
            return (
              <SingleCartItem
                key={index}
                item={item}
                shoppingCart={this.props.shoppingCart}
              />
            );
          })}
        </List>
      </div>
    );
  }
}

const filterCategories = items => {
  let filteredItems = [];

  items.forEach(item => {
    if (!filteredItems.includes(item.category)) {
      filteredItems.push(item.category);
    }
  });

  return filteredItems;
};

const mapStateToProps = state => ({
  categories: filterCategories(state.foodItems.initialItems),
  foodItems: state.foodItems.initialItems,
  shoppingCart: state.foodItems.shoppingCart
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addFoodItems,
      removeFoodItems
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(RightPanel);
